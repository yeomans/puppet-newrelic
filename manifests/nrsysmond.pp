class newrelic::nrsysmond($license_key="$newrelic::license_key")
    {
    info("newrelic license key = $license_key.")
    
    class{'newrelic::nrsysmond::repo':} -> 
    class{'newrelic::nrsysmond::package':} ->
    class{'newrelic::nrsysmond::config': license_key=>"$license_key"} ~>
    class{'newrelic::nrsysmond::service':}
    }
