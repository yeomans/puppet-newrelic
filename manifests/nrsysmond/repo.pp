class newrelic::nrsysmond::repo()
    { 
    if $::osfamily == 'Debian'
        {
        apt::pin
            {
            'apt.newrelic.com':
            originator => 'apt.newrelic.com',
            priority   => 500,
            }->
        apt::source 
            {
            'apt.newrelic.com':
              location => 'http://apt.newrelic.com/debian/',
              release => "newrelic",
              repos => "non-free",
              key => {id => 'B60A3EC9BC013B9C23790EC8B31B29E5548C16BF', 'source' => 'https://download.newrelic.com/548C16BF.gpg'},
              include => { 'src' => false },
            }
        }
    }
