class newrelic::nrsysmond::service()
    {
    $run_dir = '/var/run/newrelic'
    
    file
        {
        "$run_dir":
        ensure => directory,
        owner => 'newrelic', 
        group => 'newrelic', 
        mode => '0755'
        }
        
    service
        {
        "newrelic-sysmond":
        ensure => running, 
        require => File["$run_dir"]
        }
    }
