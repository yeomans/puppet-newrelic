class newrelic::nrsysmond::package()
    {    
    if $::osfamily == 'Debian'
        {
        package
            {
            "newrelic-sysmond":
            ensure => installed
            }
        }
    }