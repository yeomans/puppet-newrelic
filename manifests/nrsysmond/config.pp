class newrelic::nrsysmond::config
    (
    $license_key,
    $path='/etc/newrelic/nrsysmond.cfg',
    $log_level=undef, 
    $logfile=undef, 
    $proxy=undef, 
    $ssl=undef, 
    $ssl_ca_bundle=undef, 
    $ssl_ca_path=undef, 
    $pidfile=undef, 
    $collector_host=undef,
    $timeout=undef
    )
    {
    
    file
        {
        "$path":
        ensure => file, 
        owner => 'root', 
        group => 'newrelic', 
        mode => '0640',
        content => template('newrelic/nrsysmond.cfg.erb')
        }
    }